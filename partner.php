<!DOCTYPE HTML>
<html>

<head>

    <meta charset="utf-8">

    <title> AGAFX—IB代理计划</title>
    <meta name="keywords" content="IB,经纪商,合作伙伴,代理商,介绍经纪商"/>
     <meta name="description" content="马上加入我们的介绍经纪商（IB）计划，我们会根据您介绍的客户所产生的每一个交易单子，给予您丰富和持续的佣金收入。该计划适用于所有个人以及各种规模的企业经纪人，您只需要介绍新客户加入AGAFX交易就可以获得奖励。">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />

    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- css -->
    <!-- <link rel="stylesheet" type="text/css" href="assets/css/normalize.css"> -->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/media.css">
    <!-- <link rel="stylesheet" type="text/css" href="assets/css/fonts.css"> -->
    <!-- <link rel="stylesheet" type="text/css" href="assets/css/jquery-owl-carousel/owl.carousel.css" /> -->
    <!-- <link rel="stylesheet" type="text/css" href="assets/css/jquery-loading/component.css" /> -->

    <!-- <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />

    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css" rel="stylesheet">

    <!-- load modernizer -->
    <!-- <script type="text/javascript" src="assets/js/modernizr/modernizr-2.7.1.js"></script> -->

    <style>
        .art_style{font-family: "微软雅黑";}
        .container h3{line-height: 50px;font-weight: 700;font-family: "微软雅黑";}
        .container h4{line-height: 50px;font-weight: 700;font-family: "微软雅黑";}
        .container p{line-height: 30px;font-family: "微软雅黑";}

        .adv{background: url(assets/img/partner/adv_bg.jpg);}
        .serve ul {list-style-type: square;margin-left: 20px; margin-top: 50px; margin-bottom: 50px;}
        .serve ul li{font-weight: 700;height: 40px;}
        .z9{z-index: 9;}

        @media (max-width: 992px) {
            .max992-mc{margin: 0 auto;}
        }
    </style>

</head>

<body>


    <div id="wrapper">

        <!-- header -->
        <?php include 'header.html'; ?>

        <div class="slide h300 bcfff  p-r">
            <h2 class="cfff p-a t50- tc fs40  ffwryh">合作伙伴</h2>
        </div>

        <!-- <div class=" h500">


        </div>
 -->

        <div class="container  tc pt100 pb80" >

            <figure>
                <img src="assets/img/partner/02.png" alt="" class="w100-">
            </figure>
            <div class="art_style">
                <h3 class=" mt30">客户交易可获得佣金</h3>
                <div class="tl">
                    <p class="ti2">马上加入我们的介绍经纪商（IB）计划，我们会根据您介绍的客户所产生的每一个交易单子，给予您丰富和持续的佣金收入。</p>
                    <p class="ti2">AGA安格国际高度重视您所带来的业务，并致力于为我们的合作伙伴提供最具有吸引力的IB经纪回报，同时为您的客户带来市场领先的交易经验以及最为透明化的交易条件。</p>
                    <p class="ti2">该计划适用于所有个人以及各种规模的企业经纪人，您只需要介绍新客户加入AGA安格国际交易就可以获得奖励。</p>
                </div>
            </div>

        </div>
        <div class="adv tc">
            <div class="container ">
                <div class="row  pb50">
                    <div class="col-xs-6 col-md-3">
                        <figure>
                            <img src="assets/img/partner/11.png" alt="" class="mt50">
                        </figure>
                        <h4>极具吸引力的佣金</h4>
                        <p class="tl">我们的佣金回报结构经过精心设计，以便我们的各级经纪人获得充分的奖励 - 客户交易的数额越多，您赚取的佣金就越多。</p>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <figure>
                            <img src="assets/img/partner/12.png" alt="" class="mt50">
                        </figure>
                        <h4>先进的报表</h4>
                        <p class="tl">先进的IB管理中心系统保证各级代理获得完全可视并且及时更新的佣金收入情况和客户交易明细，并且及时导出报表，方便管理。</p>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <figure>
                            <img src="assets/img/partner/13.png" alt="" class="mt50">
                        </figure>
                        <h4>先进的营销工具</h4>
                        <p class="tl">我们向您提供免费的营销工具和材料，包括各种图标和网页素材，为您定制个性化的申请链接。方便您及时跟踪每一个用户的点击，展示，下载和申请。</p>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <figure>
                            <img src="assets/img/partner/14.png" alt="" class="mt50">
                        </figure>
                        <h4>高级客户经理支持</h4>
                        <p class="tl">作为我们重要的合作伙伴，您将拥有个人专属的一对一IB客户经理为您每一步操作提供业务指导，并且提供业务发展后续支持。</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="form">
            <div class="container ">
                <div class="row max1440-w850 dib pt50 pb50">
                    <div class="col-xs-12 col-md-6 ">
                        <img src="assets/img/partner/03.png" alt="" class="wow w100- max1440-w110">
                    </div>

                    <div class="col-xs-12 col-md-5 col-md-offset-1 tl wow ">
                        <div style="width: 194px;height: 76px;" class="max992-mc">
                            <img src="assets/img/partner/04.png" alt="" class="mt20 max1440-w80 ">
                        </div>

                        <form action="php/mail.php" method="post" class="mt50  w100-" id="mailForm">
                            <div class="row  ptb5 max1440-ptb0">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <input type="text" name="name" id="name" class="form-control bgn"  placeholder="姓名">
                                    </div>
                                </div>

                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <input type="tel" name="tel" id="tel" class="form-control bgn" placeholder="手机">
                                    </div>
                                </div>
                            </div>
                            <div class=" ptb5 max1440-ptb0">
                                <div class="form-group">
                                    <input type="email" name="email" id="email" class="form-control bgn" placeholder="邮箱">
                                </div>
                            </div>
                            <div class="ptb5 max1440-ptb0">
                                <div class="form-group">
                                    <textarea class="form-control bgn" name="need" id="need" rows="3" placeholder="您的需求"></textarea>
                                </div>
                            </div>
                            <div class="row  ptb5 max1440-ptb0">
                                <div class="col-xs-6">
                                    <button type="submit" class=" btn btn-default form-control bc333 bn cfff " > 提交 </button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>

                <div>
                    <img src="assets/img/partner/line.png" alt="" class="w100-">
                </div>
                <h3 class="mt50">您的客户将获得一下服务</h3>
                <p>当您成为我们的合作伙伴后，您和您的客户将会获得AGA安格国际提供的最为优质的服务。</p>
                <div class="row  serve">
                    <div class="col-xs-12 col-sm-6">
                        <ul>
                            <li>最具宽度的全球交易市场</li>
                            <li>好评无数的平台交易系统</li>
                            <li>强大的流动性和执行速度</li>
                            <li>24小时专业客服技术支持</li>
                            <li>与最可信赖的合规经纪商一起交易</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 p-r hidden-xs">
                        <figure class="p-a l-40 w500 t-100">
                            <img src="assets/img/partner/05.png" alt="" class="w100-">
                        </figure>
                    </div>
                </div>

            </div>
        </div>

        <!-- footer -->
        <?php include 'footer.html'; ?>

    </div> <!-- wrapper -->

    <!-- js -->
    <!-- <script type="text/javascript" src="assets/js/jquery/jquery.min.js"></script> -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- <script src="https://cdn.bootcss.com/jquery-validate/1.17.0/jquery.validate.min.js"></script> -->
    <script src="https://cdn.bootcss.com/jquery-validate/1.9.0/localization/messages_cn.min.js"></script>
    <script src="assets/js/jw-base.js"></script>
    <script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
    <!-- <script src="assets/js/appAjax.js"></script> -->
    <script>
        $(function(){
            $("#mailForm").bootstrapValidator({
                message: 'This value is not valid',
            　　validfeedbackIcons: {
            　　　　　　　　valid: 'glyphicon glyphicon-ok',
            　　　　　　　　invalid: 'glyphicon glyphicon-remove',
            　　　　　　　　validating: 'glyphicon glyphicon-refresh'
            　　　　　　　　},
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: '用户名不能为空'
                            },
                            stringLength: {
                                min: 2,
                                max: 6,
                                message: '请输入正确的姓名'
                            }

                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: '邮箱地址不能为空'
                            },
                            emailAddress: {
                                message: '邮箱地址格式有误 '
                            }
                        }
                    },
                    tel: {
                        validators: {
                            notEmpty: {
                                message: '电话号码不能为空'
                            },
                            stringLength: {
                                 min: 11,
                                 max: 11,
                                 message: '请输入正确的手机号码'
                             },
                             regexp: {
                                 regexp: /^1[3|5|8]{1}[0-9]{9}$/,
                                 message: '请输入正确的手机号码'
                            }
                        }
                    },
                    need: {
                        validators: {
                            notEmpty: {
                                message: '需求不能为空'
                            },
                            stringLength: {
                                min: 2,
                                max: 100,
                                message: '需求长度必须大于5位小于100字符'
                             }

                        }
                    }
                }
            })
        })
    </script>

</body>
</html>
