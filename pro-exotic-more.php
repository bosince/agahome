<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title> AGAFX—货币交易品种一览表</title>
    <meta name="keywords" content="外汇稀有盘,交易品种,货币对"/>
     <meta name="description" content=" AGAFX采用浮动点差，客户能在AGAFX平台中体验到令人兴奋的至低点差，且无重复报价。此外，AGAFX还能提供高达100：1的灵活杠杆，有效帮助交易者实现利益最大化。">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style> 
        h2{font-size: 35px;}
        p{line-height: 32px;font-size: 15px;color: #666}
        .table th, .table td{text-align: center;}

    </style>

</head>

<body>
    
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>
        <div class="container">
            <ol class="breadcrumb bcfff lh50 mb0">
                <li><a href="/">首页</a></li>
                <li><a href="/pro-exotic.php">外汇稀有盘</a></li>
                <li class="active">货币交易品种一览表</li>
            </ol>
        </div>

        <div class="data-box pt80 pb80">
            <div class="container">
                <h2 class="tc">货币交易品种一览表</h2>
                <p class="tc plr15 mt30">AGA安格国际采用浮动点差，客户能在AGA安格国际平台中体验到令人兴奋的至低点差，且无重复报价。此外，AGA安格国际还能提供高达100：1的灵活杠杆，有效帮助交易者实现利益最大化。</p>
                 <div class="table-responsive">

                <table class="table table-bordered table-striped table-hover c666 mt30" >
                    <tbody>
                        <tr>
                            <td>交易货币对</td>
                            <td>最小交易手数</td>
                            <td>最大交易手数</td>
                            <td>合约单位</td>
                            <td>挂单距离</td>
                            <td>保证金比例<br>（视账户净值及余额情况而定）</td>
                            <td>交易时间（北京时间）</td>
                            <td>交易时间（MT4时间）</td>
                        </tr>                   
                        <tr>
                            <td>USD/MXN</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>49.9</td>
                            <td>0.5%~2%</td>
                            <td>周一至周五：07:00-05:59 </td>
                            <td>周一至周五：01:00-24:00 </td>
                        </tr>
                        <tr>
                            <td>USD/HKD</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>49.9</td>
                            <td>5%~20%</td>
                            <td>周一至周五：06:05-05:59 </td>
                            <td>周一至周五：00:05-24:00 </td>
                        </tr>
                        <tr>
                            <td>USD/CNH</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>49.9</td>
                            <td>5%~20%</td>
                            <td>周一至周五：06:05-05:59 </td>
                            <td>周一至周五：00:05-24:00 </td>
                        </tr>
                        <tr>
                            <td>EUR/SEK</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>9.9</td>
                            <td>1%~4%</td>
                            <td>周一至周五：07:00-05:59 </td>
                            <td>周一至周五：01:00-24:00 </td>
                        </tr>
                        <tr>
                            <td>EUR/NOK</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>9.9</td>
                            <td>1%~4%</td>
                            <td>周一至周五：07:00-05:59 </td>
                            <td>周一至周五：01:00-24:00 </td>
                        </tr>
                        <tr>
                            <td>USD/SEK</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>9.9</td>
                            <td>1%~4%</td>
                            <td>周一至周五：07:00-05:59 </td>
                            <td>周一至周五：01:00-24:00 </td>
                        </tr>
                        <tr>
                            <td>USD/DKK</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>9.9</td>
                            <td>1%~4%</td>
                            <td>周一至周五：07:00-05:59 </td>
                            <td>周一至周五：01:00-24:00 </td>
                        </tr>
                        <tr>
                            <td>NOK/SEK</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>9.9</td>
                            <td>1%~4%</td>
                            <td>周一至周五：07:00-05:59 </td>
                            <td>周一至周五：01:00-24:00 </td>
                        </tr>
                        <tr>
                            <td>USD/ZAR</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>9.9</td>
                            <td>1%~4%</td>
                            <td>周一至周五：07:00-05:59 </td>
                            <td>周一至周五：01:00-24:00 </td>
                        </tr>
                        <tr>
                            <td>USD/NOK</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>9.9</td>
                            <td>1%~4%</td>
                            <td>周一至周五：07:00-05:59 </td>
                            <td>周一至周五：01:00-24:00 </td>
                        </tr>
                        <tr>
                            <td>EUR/TRY</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>9.9</td>
                            <td>1%~4%</td>
                            <td>周一至周五：07:00-05:59 </td>
                            <td>周一至周五：01:00-24:00 </td>
                        </tr>
                        <tr>
                            <td>USD/TRY</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>9.9</td>
                            <td>1%~4%</td>
                            <td>周一至周五：07:00-05:59 </td>
                            <td>周一至周五：01:00-24:00 </td>
                        </tr>
                        <tr>
                            <td>USD/SGD</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>9.9</td>
                            <td>1%~4%</td>
                            <td>周一至周五：07:00-05:59 </td>
                            <td>周一至周五：01:00-24:00 </td>
                        </tr>
                    </tbody>
                    
                </table>
                </div>

                <p>注：*美国夏令时，北京交易时间相应提前1小时<br>AGA安格国际 提醒您考虑提高杠杆率的风险。市场上相对较小的波动可能按比例放大，对您已存入或将要存入的资金产生较大影响，这可能对您不利，也可能对您有利。您可能损失全部原始保证金，并需要存入额外资金来补仓。</p>
                
            </div>
        </div>

        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>