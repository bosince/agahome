<!DOCTYPE HTML>
<html>

<head>

    <meta charset="utf-8">

    <title>白标合作</title>
    <meta name="keywords" content="白标合作,金融监管牌照,流动性提供商,透明报价,超快速开户,畅通银联出入金、快速指令执行 "/>
     <meta name="description" content=" AGAFX总部,位于欧洲最大的经济中心——英国伦敦,作为世界上最大的国际外汇市场 和世界上最大的离岸美元、离岸欧元市场,美元和欧元正是在这里定价,全球41%的货币业务都是在伦敦交易完成！">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />

    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />

    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css" rel="stylesheet">

    <!-- load modernizer -->
    <!-- <script type="text/javascript" src="assets/js/modernizr/modernizr-2.7.1.js"></script> -->

    <style>
        
        section{padding-top: 20px; padding-bottom: 20px;}

        #head>h4{
            font-family: "黑体";
            font-size: 26px;
            margin-top: 40px;
            color: #8d141b;
            line-height: 60px;
        }
        .container h4{
            font-size: 20px;
            line-height: 40px;
            font-weight: 700;
            margin-top: 30px;
        }
        .container p, .container li{
            font-size: 15px;
            line-height: 25px;
        } 
        .container figure{
            text-align: center;
        }
        @media (max-width: 768px){
            .container figure img {width: 100%;}
        }
        

    </style>

</head>

<body>


    <div id="wrapper">

        <!-- header -->
        <?php include 'header.html'; ?>

        <div class="slide h300 bcfff tc pt120">
            <h2 class="cfff fs40  ffwryh">白标合作</h2>
        </div>



        <div  >
        <div class="container  ">
            <section id="head">
                <h4>AGA安格国际白标与流动性解决方案</h4>
                <p>AGA安格国际的多银行流动性对经纪商及MT4、MT5运营平台均已开放。我们已经将多个流动性服务商聚合为单一供应，可直接传输至MetaTrader基础设备。</p>
            </section>

            <section >
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <h4>优点</h4>
                        <ul>
                            <li>有竞争力的盈利共享模式</li>
                            <li>低点差</li>
                            <li>实时监控功能</li>
                            <li>可定制的后台办公室包括CRM</li>
                            <li>自营和第三方市场研究</li>
                            <li>教育培训工具和资料</li>
                            <li>优化搜索及网页开发</li>
                            <li>MT4和网页版平台支持</li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <h4>系统安全</h4>
                        <p>该网上交易平台处理下列问题时，主要通过使用128位加密，以及针对不同用户在交易流程中查看与上传时的权限需要的密码和相关限制：</p>
                        <ul>
                            <li>员工的蓄意操作</li>
                            <li>管理机构的蓄意操作</li>
                            <li>经理人与经纪商的操作失误</li>
                            <li>客户试图经由网络攻击系统</li>
                            <li>计算机设备性能出现问题</li>
                            <li>从数据源提供的报价或价格所出现的问题</li>
                        </ul>

                    </div>
                </div>
            </section>

            <section>
                <figure><img src="assets/img/white-labels1.jpg" alt=""></figure>
            </section>

            <section>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <h4>多银行流动性</h4>
                        <p>通过连接AGA安格国际的流动性桥，MT4经纪商可从稳定安全的价位获得深度且可靠的流动性，进而获得较佳的价格执行，从而提高客户的留存度。</p>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <h4>风险管理</h4>
                        <p>AGA安格国际的白标合作商与AGA安格国际及我们的流动性服务商合作，可选择抵消所有、一些或不抵消市场风险。这样一来，可拥有更多的灵活性选择，作为代理可以最小化风险，作为市商则可以调整自己的商业模式。</p>
                    </div>
                </div>
            </section>

            <section>
                <figure><img src="assets/img/white-labels2.jpg" alt=""></figure>
            </section>

            <section>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <h4>白标和流动性支持服务</h4>
                        <p>AGA安格国际提供每周5天、每天24小时的外汇及衍生品专业服务。我们为您提供深度的服务及专业的风险管理知识，不论您是机构、银行抑或是经纪商，AGA安格国际总能为您提供量身打造的流动性解决方案支持。我们与行业领先的专有技术开发商合作，针对客户在金融服务及网上交易行业的具体需求，提供正确的解决方案。</p>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <h4>监管</h4>
                        <p>AGA安格国际承诺完全按照其牌照经营，遵守英国法律及其它适用的法规章程。同时在FCA批准的AA级存款机构英国国民银行设有独立的客户信托账户。公司每年都进行独立的审计，确保遵守英国公司法及处理客户资金的牌照条件。</p>
                    </div>
                </div>
            </section>

            <section>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <form class="form-horizontal"  action="php/mail.php" method="post" id="mailForm">
                            <div class="form-group">
                                <label for="inputName" class="col-sm-2 control-label">姓名</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="name" id="inputName" placeholder="姓名">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail" class="col-sm-2 control-label">邮箱</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" name="email" id="inputEmail" placeholder="邮箱">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputTel" class="col-sm-2 control-label">电话</label>
                                <div class="col-sm-10">
                                    <input type="tel" class="form-control" name="tel" id="inputTel" placeholder="电话">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputNeed" class="col-sm-2 control-label">需求</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control bgn" name="need" id="inputNeed" rows="3" placeholder="您的需求"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <input type="submit" class="form-control btn-info" id="inputSubmit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>

        <!-- footer -->
        <?php include 'footer.html'; ?>

    </div> <!-- wrapper -->

    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>

    <script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>

    <script>
        $(function(){
            $("#mailForm").bootstrapValidator({
                message: 'This value is not valid',
            　　validfeedbackIcons: {
    　　　　　　　　valid: 'glyphicon glyphicon-ok',
    　　　　　　　　invalid: 'glyphicon glyphicon-remove',
    　　　　　　　　validating: 'glyphicon glyphicon-refresh'
　　　　　　　　},
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: '用户名不能为空'
                            },
                            stringLength: {
                                min: 2,
                                max: 6,
                                message: '请输入正确的姓名'
                            }
                            
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: '邮箱地址不能为空'
                            },
                            emailAddress: {
                                message: '邮箱地址格式有误'
                            }
                        }
                    },
                    tel: {
                        validators: {
                            notEmpty: {
                                message: '电话号码不能为空'
                            },
                            stringLength: {
                                 min: 11,
                                 max: 11,
                                 message: '请输入11位手机号码'
                             },
                             regexp: {
                                 regexp: /^1[3|5|8]{1}[0-9]{9}$/,
                                 message: '请输入正确的手机号码'
                            }
                        }
                    },
                    need: {
                        validators: {
                            notEmpty: {
                                message: '需求不能为空'
                            },
                            stringLength: {
                                min: 2,
                                max: 100,
                                message: '需求长度必须大于5位小于100字符'
                             }

                        }
                    }
                }
            })
        })
    </script>

</body>
</html>
