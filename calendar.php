<!DOCTYPE HTML>
<html>

<head>

	<meta charset="utf-8">

	<title>财经日历</title>
    <meta name="keywords" content="关于我们,金融监管牌照,流动性提供商,透明报价,超快速开户,畅通银联出入金、快速指令执行 "/>
     <meta name="description" content=" AGAFX总部,位于欧洲最大的经济中心——英国伦敦,作为世界上最大的国际外汇市场 和世界上最大的离岸美元、离岸欧元市场,美元和欧元正是在这里定价,全球41%的货币业务都是在伦敦交易完成！">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />

	<!-- favicon -->
	<link rel="shortcut icon" href="assets/img/favicon.png">

	<link href="//cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />

	<!-- load modernizer -->
	<script type="text/javascript" src="assets/js/modernizr/modernizr-2.7.1.js"></script>

	<style>
		@media (max-width: 1440px) {
		    .max1440-w920{width: 920px;}

		}

	</style>

</head>

<body>


	<div id="wrapper">

		<!-- header -->
		<?php include 'header.html'; ?>

		<!-- <div class="slide h300 bcfff tc pt120">
			<h2 class="cfff fs40  ffwryh">公司介绍</h2>
		</div> -->

        <div class="container  ">
			<h1 class="fs35 c333 tc mt50">财经日历</h1>
            <iframe id="iframe"  height="1080"  width="100%"  frameborder="0"  scrolling="yes"  src="https://rili-d.jin10.com/open.php"></iframe>
        </div>
    </div>

		<!-- footer -->
		<?php include 'footer.html'; ?>

	</div> <!-- wrapper -->

	<!-- js -->
	<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>

</body>
</html>
