<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title>MT4软件介绍 - AGA</title>
    
    <!-- meta -->
    <meta name="author" content="Jimmy">
    <meta name="description" content="AGA ">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style>
        .ul1{list-style-type: square;}
        .ul1 li{line-height: 25px;}
        .mt4-1{top:0;left:40px;}
        .mt4-2{top:150px;left:-38px;}
        .mt4-3{top:368px;left:0px;}
        .mt4-4{top:368px;left:410px;}
        .mt4-5{top:368px;left:780px;}
        .mt4-6{top:150px;left:793px;}
        .mt4-7{top:0;left:793px;}

        @media (max-width: 1440px) {
            .mt4-1{top:-30px;left:-5px;width: 280px;}
            .mt4-2{top:120px;left:-75px;width: 350px;}
            .mt4-3{top:290px;left:0px;width: 340px;}
            .mt4-4{top:290px;left:310px;width: 365px;}
            .mt4-5{top:290px;left:580px;width: 312px;}
            .mt4-6{top:120px;left:615px;width: 320px; }
            .mt4-7{top:-20px;left:615px;width: 280px;}

            .max1440-w200{width: 200px;}
            .max1440-w400{width: 400px;}
        }

   </style>
</head>

<body>
    
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>

        <div  style="background-color: #f1f1f1;" >
            <div class="container tc pt100   max1440-w920">
                <img src="assets/img/mt4/01.png" alt="" class="max1440-w600">
                <div class="p-r mt50 pt50 pb150 min1200">
                    <img src="assets/img/mt4/11.png" alt="" class="max1440-w450">
                    <img src="assets/img/mt4/02.png" alt="" class="p-a mt4-1">
                    <img src="assets/img/mt4/03.png" alt="" class="p-a mt4-2">
                    <img src="assets/img/mt4/04.png" alt="" class="p-a mt4-3">
                    <img src="assets/img/mt4/05.png" alt="" class="p-a mt4-4">
                    <img src="assets/img/mt4/06.png" alt="" class="p-a mt4-5">
                    <img src="assets/img/mt4/07.png" alt="" class="p-a mt4-6">
                    <img src="assets/img/mt4/08.png" alt="" class="p-a mt4-7">
                </div>
                <div class="mt80 mb100">
                    <a href="javascript:void(0);"><img src="assets/img/mt4/12.png" alt="" class="max1440-w400"></a>
<!--                     <a href="javascript:void(0);"><img src="assets/img/mt4/10.png" alt="" class="max1440-w200"></a>
                    <a href="javascript:void(0);"><img src="assets/img/mt4/09.png" alt="" class="max1440-w200"></a> -->
                </div>
                
            </div>
        </div>

        
        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>