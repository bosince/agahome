<!DOCTYPE HTML>
<html>

<head>

	<meta charset="utf-8">

	<title>AGAFX—关于我们</title>
    <meta name="keywords" content="关于我们,金融监管牌照,流动性提供商,透明报价,超快速开户,畅通银联出入金、快速指令执行 "/>
     <meta name="description" content=" AGAFX总部,位于欧洲最大的经济中心——英国伦敦,作为世界上最大的国际外汇市场 和世界上最大的离岸美元、离岸欧元市场,美元和欧元正是在这里定价,全球41%的货币业务都是在伦敦交易完成！">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />

	<!-- favicon -->
	<link rel="shortcut icon" href="assets/img/favicon.png">

	<link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />

	<!-- load modernizer -->
	<script type="text/javascript" src="assets/js/modernizr/modernizr-2.7.1.js"></script>

	<style>
		@media (max-width: 1440px) {
		    .max1440-w920{width: 920px;}

		}

	</style>

</head>

<body>


	<div id="wrapper">

		<!-- header -->
		<?php include 'header.html'; ?>

		<div class="slide h300 bcfff tc pt120">
			<h2 class="cfff fs40  ffwryh">公司介绍</h2>
		</div>



		<div  >
        <div class="container  ">
            <!-- <h4 class="fs30 fw7 pt30 mb30 lh60 c333 ffht  tc">关于我们</h4> -->
            <div class="ffwryh c666 mt80">
                <p class="lh30 ">AGA安格总部，位于欧洲最大的经济中心——英国伦敦，作为世界上最大的国际外汇市场 和世界上最大的离岸美元、离岸欧元市场，美元和欧元正是在这里定价，全球41%的货币业务都是在伦敦交易完成！</p>
                <p class="lh30 ">作为全球领先的外汇、差价合约（CFDs）、商品期货的持牌供应商，拥有完善的金融监管体系和严格的执行力度，AGA安格国际获得了英国FCA金融监管牌照，美国国家期货协会NFA的金融监管牌照，并且已加入英国金融服务补偿计划FSCS，一直以来都被公认为世界上最严格、最健全，最能保护投资者权益的金融监管体系！</p>
                <p class="lh30 ">如果投资者在交易中发生意外，将获得五万英镑的补偿，并且有机会将此补偿全额上调！</p>
                <p class="lh30 ">AGA安格国际为更好的服务全球客户，下设办事机构遍布英国、美国、香港、澳大利亚等全球多个国家或地区，提供十余种世界主要语言在线咨询和服务。</p>
                <p class="lh30 ">AGA安格国际拥有全球顶尖流动性提供商，包括英国巴克莱银行、摩根银行、花旗银行、美国银行、汇丰银行、法国兴业银行、高盛集团等！</p>
                <p class="lh30 ">AGA安格国际的核心团队，是行业顶尖金融从业人员，曾服务于各大国际金融机构，具备超高的专业素质和超高金融从业经验！</p>
                <p class="lh30 ">AGA安格国际坚持自主研发，交易平台包括个人电脑、智能手机、平板电脑等各个终端，个性化的操作界面，可靠的安全化保障，让您足不出户，就可以安心尽享全球的财富资源！</p>
                <p class="lh30 ">AGA安格国际致力于为投资者，提供一站式服务和顶尖的交易环境。</p>
                <p class="lh30 ">精准可靠的透明报价，确保您获得最优的交易价格，超快速开户、畅通银联出入金、快速指令执行，让交易者获得最大的收益。</p>
                <p class="lh30 ">AGA安格国际坚信“百年专注做好一件事”，始终坚持客户第一，以诚信待人，共同推动金融衍生品投资市场，一起踏向无限未来，共赢天下财富！</p>
				<div class="mt15">
                    <p class="lh30 ffwryh fs16"> <span class="glyphicon glyphicon-envelope"></span> INFO@AGAFX.COM</p>
                    <p class="lh30 ffwryh fs16"> <span class="glyphicon glyphicon-earphone"></span> 0044-20-3239-3600</p>
                    <p class="lh30 ffwryh fs16"> <span class="glyphicon glyphicon-send"></span> RM101, MAPLE HOUSE 118 HIGH STREET PURLEY, LONDON UNITED KINGDOM CR8 2AD</p>
                </div>
            </div>
            <div class="mt50 mb80 tc">
              <img src="assets/img/about/about_bg_cn.png" alt="" class="w100-">
            </div>
        </div>
    </div>

		<!-- footer -->
		<?php include 'footer.html'; ?>

	</div> <!-- wrapper -->

	<!-- js -->
	<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>

</body>
</html>
