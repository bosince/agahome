<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title> AGAFX—外汇稀有盘</title>
    <meta name="keywords" content="外汇稀有盘,USDZAR,EURTRY,USDMXN"/>
     <meta name="description" content="AGAFX外汇稀有盘包含了特殊国家提供的货币，它不像主流盘那样交投广泛，但是却具有特殊投资价值，它能够满足交易者的特定交易需求。这些稀有货币对通常与一种主流货币进行组合，诸如美元/南非兰特（USD/ZAR），欧元/土耳其里拉（EUR/TRY）、美元/墨西哥比索（USD/MXN）等">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style> 
        .introduce{background: url(assets/img/pro/exotic01.jpg) no-repeat 0 100%;background-size: cover; height: 560px;padding-top: 110px;}
        h2{font-size: 35px;}
        p{line-height: 32px;font-size: 15px;color: #666}

        .platform ul li{line-height: 55px; padding-left: 50px; background: url(assets/img/pro/icon.png) no-repeat left 9px;}

        .account-intro{background: url(assets/img/pro/exotic03.jpg) no-repeat 30%;background-size: cover;height: 500px;}

        @media (max-width: 1440px) {
            .max1440-w920{width: 920px;}
        }

        .bc143a89{background-color: #143a89;}
        .sp-action{background-color: #eee;color: #143a89;}
        .sp-btn-hov:hover{background-color: #eee;color: #143a89;}

        .table th, .table td{text-align: center;}

        @media (max-width: 768px) {
            .max768-tc{text-align: center;}  
            .max768-pl150{padding-left: 150px;}
            .max768-lh28{line-height: 28px;}

            .introduce{
                padding-top: 80px;
            }
        }


       

    </style>

</head>

<body>
    
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>
        <div class="container">
            <ol class="breadcrumb bcfff lh50 mb0">
                <li><a href="/">首页</a></li>
                <!-- <li><a href="#">首页</a></li> -->
                <li class="active">外汇稀有盘</li>
            </ol>
        </div>
        <div class="introduce cfff ">
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 col-md-6">    
                        <h2 class="tl">外汇稀有盘</h2>
                        <p class="cfff mt50 max768-lh28">外汇稀有盘包含了特殊国家提供的货币，它不像主流盘那样交投广泛，但是却具有特殊投资价值，它能够满足交易者的特定交易需求。这些稀有货币对通常与一种主流货币进行组合，诸如美元/南非兰特（USD/ZAR），欧元/土耳其里拉（EUR/TRY）、美元/墨西哥比索（USD/MXN）、等，但同时也存在两种稀有货币组合的情况，如挪威克朗/瑞典克朗（NOK/SEK）等。这些货币通常在地缘经济上占有一定优势，如墨西哥作为中美洲的经济体系的领头羊，主导着中美洲经济命脉；土耳其作为中东货币，起到稳定、制衡中东经济的作用等，投资者可以根据自己的投资需要选择性进行交易。</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="platform container pt50 pb80"> 
            <div class="row">
                <div class="col-xs-12 col-sm-6">    
                    <figure>
                        <img src="assets/img/pro/exotic02.jpg" alt="" class="w100- mt30 mb30">
                    </figure>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <h2 class="pl50  mt30 ">为什么选择外汇稀有盘</h2>
                    <ul class="pl50 mt50  fs20 c666">
                        <li>特殊经济用途和价值</li>
                        <li>双向交易灵活避险</li>
                        <li>适用于本国交易者进行交易</li>
                        <li>新兴市场崛起，投资潜力大</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="data-box pt80 pb80" style="background-color: #f1f1f1">
            <div class="container">
                <h2 class="tc">交易数据</h2>
                <p class="tc plr15 mt30">AGA安格国际可为客户提供最高达100:1的杠杆，及灵活的合约大小，多种账户类型与挂单类型，同时在点差上也具有出色的表现，客户能在交易中享受独特的交易体验。</p>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover c666 mt30" >
                        <tbody>
                            <tr>
                                <td>示例货币对</td>
                                <td>最小交易手数</td>
                                <td>最大交易手数</td>
                                <td>合约单位</td>
                                <td>挂单距离</td>
                                <td>保证金比例</td>
                                <td>交易时间（北京时间）</td>
                                <td>交易时间（MT4时间）</td>
                            </tr>
                            <tr>
                                    <td>USD/MXN</td>
                                    <td>0.01</td>
                                    <td>20</td>
                                    <td>100,000</td>
                                    <td>49.9</td>
                                    <td>0.5%~2%</td>
                                    <td>周一至周五：07:00-05:59</td>
                                    <td>周一至周五：01:00-24:00</td>
                                </tr>
                                <tr>
                                    <td>USD/HKD</td>
                                    <td>0.01</td>
                                    <td>20</td>
                                    <td>100,000</td>
                                    <td>49.9</td>
                                    <td>5%~20%</td>
                                    <td>周一至周五：06:05-05:59</td>
                                    <td>周一至周五：00:05-24:00</td>
                                </tr>
                                <tr>
                                    <td>USD/CNH</td>
                                    <td>0.01</td>
                                    <td>20</td>
                                    <td>100,000</td>
                                    <td>49.9</td>
                                    <td>5%~20%</td>
                                    <td>周一至周五：06:05-05:59</td>
                                    <td>周一至周五：00:05-24:00</td>
                                </tr>       
                        </tbody>
                    </table>
                </div>

                <p>注：*美国夏令时，北京交易时间相应提前1小时<br>AGA安格国际 提醒您考虑提高杠杆率的风险。市场上相对较小的波动可能按比例放大，对您已存入或将要存入的资金产生较大影响，这可能对您不利，也可能对您有利。您可能损失全部原始保证金，并需要存入额外资金来补仓。</p>
                
                <div class="tc mt30">
                    <a href="/pro-exotic-more.php" class="dib btn btn-default w200 h50 lh35" >查看更多货币对</a>
                </div>
            </div>
        </div>

        <div class="account-intro cfff tc">
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 col-md-6 col-md-offset-6">    
                        <h2 style="margin-top: 150px;">账户介绍</h2>
                        <p class="cfff mt50 fs20">今天，轻松获取最适合您的账户类型</p>
                        <a href="#" class="dib mt50 cfff hov-cfff">了解更多</a>
                    </div>
                </div>
            </div>
        </div>

        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>