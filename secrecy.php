<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title> AGAFX—保密协议</title>
    <meta name="keywords" content="保密协议,客户信息,个人讯息,隐私条例 "/>
     <meta name="description" content="当客户提交开户申请时，客户有可能要向AGAFX提供个人信息，AGAFX将保留所有帐户的交易和活动记录，包括交易合约的细节以及追加保证金通知。在合约关系期间，任何被客户使用过的产品和服务信息都将被记录存档。客户信息将会严格地按照英国隐私权法案中的国家隐私条例处理。">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- css -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <!-- load modernizer -->
    <!-- <script type="text/javascript" src="assets/js/modernizr/modernizr-2.7.1.js"></script> -->
    
    <style>
        .art_style h3{line-height: 50px;}
        .art_style p{line-height: 30px;}


    </style>

</head>

<body>
    
    
    <div id="wrapper">
        
        <!-- header -->
        <?php include 'header.html'; ?>

        <div class="slide h300 bcfff  tc pt120">
            <h2 class="cfff fs40  ffwryh">保密协议</h2>
        </div>


        <div class="container  ">
            <div class="ffwryh c666 art_style mt50 mb50">

                <h3>客户信息</h3>
                <p>当客户向AGA安格国际金融询问有关产品和服务，浏览AGA安格国际金融的网站，或提交开户申请时，客户有可能要向AGA安格国际金融提供个人信息。</p>

                <p>AGA安格国际金融将保留所有帐户的交易和活动记录，包括交易合约的细节以及追加保证金通知。在合约关系期间，任何被客户使用过的产品和服务信息都将被记录存档。</p>

                <p>在评估客户申请的时候，AGA安格国际金融也可能会从公共的资源里收集未来客户的信息。</p>
                <p>客户信息将会严格地按照英国隐私权法案中的国家隐私条例处理。居住在英国的客户在任何时候提出要求，都可根据以下所述的国家隐私条例取得AGA安格国际金融保存的关于他或她的个人信息。</p>


                <h3>个人讯息的使用</h3>
                <p>AGA安格国际金融在开户申请表中要求客户填写的信息是用于审核客户是否有足够的知识和经验在AGA安格国际金融进行场外金融衍生产品的交易。这些信息以及在帐户开立期间由AGA安格国际金融收集和保存的信息，都是为了让客户能获得他们帐户信息，保证金要求和交易活动的最新情况。</p>

                <p>客户在进入我们的网站或在填写我们的帐户申请表时所被要求提供的信息，是为了让我们能更好地向客户提供最适合他们投资风险承受力的产品和服务。AGA安格国际金融将采取所有合理措施来保护客户的个人信息不会被滥用，遗失，未经授权进入，修改或披露。</p>


                <h3>电话会谈</h3>
                <p>AGA安格国际金融也可能会对客户和AGA安格国际金融授权代表之间的电话会谈录音。该录音，或录音的副本，可能会用于解决任何客户的争端。由AGA安格国际金融提供的客户电话会谈的录音或录音副本，会由AGA安格国际金融自行决定是否删除。</p>


                <h3>网站</h3>
                <p>AGA安格国际金融会收集关于我们网站访客的统计讯息，如访客的数目，浏览的网页，进行过的交易类型，在线的时间以及下载过的文件。这些讯息会被用于评估和提高我们网站的品质。除了统计讯息，我们不会通过我们的网站收集任何个人信息，除非是必须向我们提供的信息。</p>


                <h3>更新个人讯息</h3>
                <p>如果客户档案中的个人信息有任何更改，AGA安格国际金融要求要立即向本公司作出通知。这样能确保AGA安格国际金融可以及时通知客户有关他们的帐户，保证金要求以及交易活动的信息。客户可以随时要求我们更正过时的或不正确的个人信息。如果我们对客户所提供信息的准确性有所质疑，客户可以要求我们对该信息附上一份声明并注明您认为该信息是不正确或不完整的。</p>


                <h3>客户同意</h3>
                <p>客户进入AGA安格国际金融的网站就代表了客户同意他们所提供的个人信息被收集、维护、使用和披露。</p>

            </div>
        </div>

        
        <!-- footer -->
        <?php include 'footer.html'; ?>
        
    </div> <!-- wrapper -->

    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="assets/js/jw-base.js"></script>
</body>
</html>