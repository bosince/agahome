<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title>Crosses of foreign exchange currency - AGA</title>
    
    <!-- meta -->
    <meta name="description" content="AGA ">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style> 
        h2{font-size: 35px;}
        p{line-height: 32px;font-size: 15px;color: #666}
        .table th, .table td{text-align: center;}

    </style>

</head>

<body>
    
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>
        <div class="container">
            <ol class="breadcrumb bcfff lh50 mb0">
                <li><a href="/en">Home</a></li>
                <li><a href="/en/pro-forex.php">G10</a></li>
                <li class="active">Crosses of foreign exchange currency</li>
            </ol>
        </div>

        <div class="data-box pt80 pb80">
            <div class="container">
                <h2 class="tc">Crosses of foreign exchange currency</h2>
                <p class="tc plr15 mt30">AGA with floating point, customers in the international AGA platform to exciting to experience low point, and not repeat the offer. In addition, AGA also provides a flexible lever to 1:100,effectively help traders to achieve maximum benefits.</p>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover c666 mt30" >
                        <tr>
                            <td>Transaction currency </td>
                            <td>Minimum number<br> of transactions</td>
                            <td>Maximum number<br> of transactions</td>
                            <td>Contract unit</td>
                            <td>Order distance</td>
                            <td>Bail scale </td>
                            <td>Business time<br>（Beijing time）</td>
                            <td>Business time<br>（MT4 time）</td>
                        </tr>
                        <tbody>
                            <tr>
                                <td>EUR/USD&nbsp;</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>2.4</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>USD/JPY&nbsp;</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>2.4</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>GBP/USD&nbsp;</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>2.4</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>USD/CHF&nbsp;</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>2.4</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>AUD/USD&nbsp;</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>2.4</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>NZD/USD&nbsp;</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>2.4</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>USD/CAD&nbsp;</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>2.4</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>EUR/GBP&nbsp;</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>EUR/CHF</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>EUR/JPY</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>2.4</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>GBP/JPY&nbsp;</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>2.4</td>
                                <td>0.5%~2%
                                </td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>CHF/JPY&nbsp;</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>2.4</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>AUD/JPY</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>2.4</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>NZD/JPY</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>2.4</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>CAD/JPY</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>AUD/NZD</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>EUR/AUD&nbsp;</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>EUR/CAD</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>GBP/CHF&nbsp;</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>GBP/AUD</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>GBP/CAD</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>GBP/NZD&nbsp;</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>EUR/NZD</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>AUD/CHF</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>19.9</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>AUD/CAD</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>19.9</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>CAD/CHF</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>19.9</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>NZD/CAD</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>19.9</td>
                                <td>0.5%~2%</td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>NZD/CHF</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>19.9</td>
                                <td>0.5%~2%
                                </td>
                                <td>Weekdays：06:05-05:59</td>
                                <td>Weekdays：00:05-24:00</td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>

                <p>Remark：Daylight saving time in the United States, Beijing trading time corresponding to 1 hours earlier<br>AGA remind you to consider raising the risk leverage. A relatively small market volatility may be scaled up, have a greater impact on you have deposited or will have to deposit funds, it may be bad for you, may also be beneficial to you. You may lose all the original deposit, and need to deposit additional funds to cover short positions.</p>
                
            </div>
        </div>

        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>