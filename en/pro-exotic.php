<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title>exotic - AGA</title>
    
    <!-- meta -->
    <meta name="description" content="AGA ">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style> 
        .introduce{background: url(assets/img/pro/exotic01.jpg) no-repeat 0 100%;background-size: cover; height: 560px;padding-top: 80px;}
        h2{font-size: 35px;}
        p{line-height: 32px;font-size: 15px;color: #666}

        .platform ul li{line-height: 55px; padding-left: 50px; background: url(assets/img/pro/icon.png) no-repeat left 9px;}

        .account-intro{background: url(assets/img/pro/exotic03.jpg) no-repeat 30%;background-size: cover;height: 500px;}

        @media (max-width: 1440px) {
            .max1440-w920{width: 920px;}
        }

        .bc143a89{background-color: #143a89;}
        .sp-action{background-color: #eee;color: #143a89;}
        .sp-btn-hov:hover{background-color: #eee;color: #143a89;}

        .table th, .table td{text-align: center;}

        @media (max-width: 768px) {
            .max768-tc{text-align: center;}  
            .max768-pl150{padding-left: 150px;}

            .max768-lh23{line-height: 23px;}
            .max768-mt15{margin-top: 15px;}

            .introduce{
                height: 620px;
            }
            .max768-pl20{padding-left: 20px;}
            .max768-fs25{font-size: 25px;}
            .max768-fs16{font-size: 16px;}
            .max768-mt20{margin-top: 20px;}

        }


       

    </style>

</head>

<body>
    
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>
        <div class="container">
            <ol class="breadcrumb bcfff lh50 mb0">
                <li><a href="/en">Home</a></li>
                <li class="active">Exotic</li>
            </ol>
        </div>
        <div class="introduce cfff ">
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 col-md-6">    
                        <h2 class="tl">Foreign exchange rare variety</h2>
                        <p class="cfff mt30 max768-lh23 max768-mt15">The foreign exchange rare variety contains a special rare state provided money, it is not widely traded as mainstream disc, but has a special investment value, it can meet the specific trading needs. These rare and usually money on a mainstream currency portfolio, such as the dollar / South African rand (USD/ZAR), the euro / lira (Turkey EUR/TRY), $/ Mexico Bissau (USD/MXN), etc., but there are also two kinds of rare currency portfolio, such as the Norway Koruna / SEK (NOK/SEK). These currencies usually occupy a certain advantage in the regional economy, as a leader in Mexico as in the American economic system, dominated in the American economy Turkey lifeline; as Middle East currency, stabilize the balance, middle east economy, investors can be traded according to their investment needs to be selective.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="platform container pt50 pb80"> 
            <div class="row">
                <div class="col-xs-12 col-sm-6">    
                    <figure>
                        <img src="assets/img/pro/exotic02.jpg" alt="" class="w100- mt30 mb30">
                    </figure>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <h2 class="pl50 max768-pl20  mt30 max768-fs25">Why choose the foreign exchange rare variety?</h2>
                    <ul class="pl50 max768-pl20  mt50 max768-mt20 fs20 max768-fs16 c666">
                        <li>Special economic use and value</li>
                        <li>Two-way trade flexible hedge</li>
                        <li>This applies to domestic traders</li>
                        <li>The rise of emerging markets, investment potential</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="data-box pt80 pb80" style="background-color: #f1f1f1">
            <div class="container">
                <h2 class="tc">transaction data</h2>
                <p class="tc plr15 mt30">The AGA can provide up to 100:1 leverage for customers, contracts and flexible size, various types and types of accounts Guadan, but also has excellent performance in it, customers can enjoy the unique transaction in the transaction experience.</p>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover c666 mt30" >
                        <tbody>
                            <tr>
                                <td>Transaction currency </td>
                                <td>Minimum number<br> of transactions</td>
                                <td>Maximum number<br> of transactions</td>
                                <td>Contract unit</td>
                                <td>Order distance</td>
                                <td>Bail scale </td>
                                <td>Business time<br>（Beijing time）</td>
                                <td>Business time<br>（MT4 time）</td>
                            </tr>
                            <tr>
                                    <td>USD/MXN</td>
                                    <td>0.01</td>
                                    <td>20</td>
                                    <td>100,000</td>
                                    <td>49.9</td>
                                    <td>0.5%~2%</td>
                                    <td>Weekdays：07:00-05:59</td>
                                    <td>Weekdays：01:00-24:00</td>
                                </tr>
                                <tr>
                                    <td>USD/HKD</td>
                                    <td>0.01</td>
                                    <td>20</td>
                                    <td>100,000</td>
                                    <td>49.9</td>
                                    <td>5%~20%</td>
                                    <td>Weekdays：06:05-05:59</td>
                                    <td>Weekdays：00:05-24:00</td>
                                </tr>
                                <tr>
                                    <td>USD/CNH</td>
                                    <td>0.01</td>
                                    <td>20</td>
                                    <td>100,000</td>
                                    <td>49.9</td>
                                    <td>5%~20%</td>
                                    <td>Weekdays：06:05-05:59</td>
                                    <td>Weekdays：00:05-24:00</td>
                                </tr>       
                        </tbody>
                    </table>
                </div>

                <p>Remark：Daylight saving time in the United States, Beijing trading time corresponding to 1 hours earlier<br>AGA remind you to consider raising the risk leverage. A relatively small market volatility may be scaled up, have a greater impact on you have deposited or will have to deposit funds, it may be bad for you, may also be beneficial to you. You may lose all the original deposit, and need to deposit additional funds to cover short positions.</p>
                
                <div class="tc mt30">
                    <a href="/en/pro-exotic-more.php" class="dib btn btn-default w200 h50 lh35" >Show More</a>
                </div>
            </div>
        </div>

        <div class="account-intro cfff tc hidden-xs">
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 col-md-6 col-md-offset-6">    
                        <h2 style="margin-top: 150px;">Account</h2>
                        <p class="cfff mt50 fs20">Now, Easy get your account type</p>
                        <a href="#" class="dib mt50 cfff hov-cfff">Read More</a>
                    </div>
                </div>
            </div>
        </div>

        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>