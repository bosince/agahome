<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title>Video - agafx</title>
    
    <!-- meta -->
    <meta name="description" content="AGA ">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">


    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <!-- load modernizer -->
    <!-- <script type="text/javascript" src="assets/js/modernizr/modernizr-2.7.1.js"></script> -->
    
    <style>
        @media (max-width: 1440px) {
            .max1440-w920{width: 920px;}

        }

        .bc143a89{background-color: #143a89;}
        .bc565656{background-color: #565656;}
        .sp-action{background-color: #eee;color: #143a89;}
        .sp-btn-hov:hover{background-color: #eee;color: #143a89;}

        .page7-1{
            margin-top: 50px;
            margin-bottom: 30px;
            width: 866px;
            height: 709px;
            background: url(assets/img/video/video_bg.png) no-repeat;
        }
        .page7-1-1{
            width: 792px;
            height: 446px;
            position: relative;
            margin-top: 44px;
            border-radius: 3px;
            overflow: hidden;
        }

        #modal-overlay {
             visibility: hidden;    
             position: absolute;   
             left: 0px;    
             top: 0px;
             width:100%;
             height:100%;
             text-align:center;
             z-index: 1000;
             background-color: #333; 
             opacity: 0.5;   
        }
        .modal-data{
             width:300px;
             margin: 100px auto;
             background-color: #fff;
             border:1px solid #000;
             padding:15px;
             text-align:center;
        }
        
    </style>

</head>

<body>
    
    
    <div id="wrapper">
        
        <!-- header -->
        <?php include 'header.html'; ?>

        <div class="slide h300 bcfff tc pt120">
            <h2 class="cfff  fs40 mc  ffwryh">Videos</h2>
        </div>


        
        <div  style="background-color: #f1f1f1;" >
            <div class=" tc pb100 pt50">
                <div class="dib wow ">
                    <div class=" dib mc">
                        <img src="assets/img/video/spbj.png" alt="" class="w100-" id="spbj" data-toggle="modal" data-target="#modal_cpxc">
                    </div>
                </div>

<!--                 <div class=" bc565656  w1000 mc fs20">
                    <div class="row">
                        <div class="col-xs-2 col-xs-offset-2 ptb20">
                            <button class="w150 b1sccc bgn br3 ptb5 cccc sp-btn-hov sp-action" id="cpxc">Business</button>
                        </div>
                        <div class="col-xs-2 col-xs-offset-1 ptb20">
                            <button class="w150 b1sccc bgn br3 ptb5 cccc sp-btn-hov" id="cpjs">Product</button>
                        </div>
                        <div class="col-xs-2 col-xs-offset-1 ptb20">
                            <button class="w150 b1sccc bgn br3 ptb5 cccc sp-btn-hov">Advantage</button>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
        
        <!-- footer -->
        <?php include 'footer.html'; ?>
      <div class="modal fade bs-example-modal-lg" id="modal_cpxc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-body">

                <video src="http://%77%77%77%2E%62%6B%75%78%75%2E%74%6F%70/bf471056792d4a81b56bd3ecb9d7a35e/03313f916e0546378a0c81e1440ee3be-5287d2089db37e62345123a1be272f8b.%6D%70%34" controls="controls" width="100%" ></video>
                <!-- <video src="//cloud.video.taobao.com//play/u/871726787/p/2/e/6/t/1/50034692361.mp4" controls="controls" width="100%" id="360videoname"></video> -->

          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
<!--     <div class="modal fade bs-example-modal-lg" id="modal_cpjs" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <embed src='http://player.youku.com/player.php/sid/XMjkwNzkwMDg1Ng==/v.swf' allowFullScreen='true' quality='high' width='100%' height='560' align='middle' allowScriptAccess='always' type='application/x-shockwave-flash'></embed>
          </div>
        </div>
      </div>
    </div> -->

        
        
    </div> <!-- wrapper -->




    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>

    <script>
      $(function(){
          $('#modal_cpxc').on("hide.bs.modal", function(){
              $(this).find("video")[0].pause();
          });
      })
    </script>

</body>
</html>