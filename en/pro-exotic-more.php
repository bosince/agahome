<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title>Currency trading varieties - AGA</title>
    
    <!-- meta -->
    <meta name="description" content="AGA ">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style> 
        h2{font-size: 35px;}
        p{line-height: 32px;font-size: 15px;color: #666}
        .table th, .table td{text-align: center;}

    </style>

</head>

<body>
    
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>
        <div class="container">
            <ol class="breadcrumb bcfff lh50 mb0">
                <li><a href="/en">Home</a></li>
                <li><a href="/en/pro-exotic.php">Exotic</a></li>
                <li class="active">Currency trading varieties</li>
            </ol>
        </div>

        <div class="data-box pt80 pb80">
            <div class="container">
                <h2 class="tc">Currency trading varieties</h2>
                <p class="tc plr15 mt30">AGA with floating point, customers in the international AGA platform to exciting to experience low point, and not repeat the offer. In addition, AGA also provides a flexible lever to 1:100,effectively help traders to achieve maximum benefits.</p>
                <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover c666 mt30" >
                    <tbody>
                        <tr>
                            <td>Transaction currency </td>
                            <td>Minimum number of<br> transactions</td>
                            <td>Maximum number of<br> transactions</td>
                            <td>Contract unit</td>
                            <td>Order distance</td>
                            <td>Bail scale </td>
                            <td>Business time<br>（Beijing time）</td>
                            <td>Business time<br>（MT4 time）</td>
                        </tr>                   
                        <tr>
                            <td>USD/MXN</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>49.9</td>
                            <td>0.5%~2%</td>
                            <td>Weekdays：07:00-05:59 </td>
                            <td>Weekdays：01:00-24:00 </td>
                        </tr>
                        <tr>
                            <td>USD/HKD</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>49.9</td>
                            <td>5%~20%</td>
                            <td>Weekdays：06:05-05:59 </td>
                            <td>Weekdays：00:05-24:00 </td>
                        </tr>
                        <tr>
                            <td>USD/CNH</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>49.9</td>
                            <td>5%~20%</td>
                            <td>Weekdays：06:05-05:59 </td>
                            <td>Weekdays：00:05-24:00 </td>
                        </tr>
                        <tr>
                            <td>EUR/SEK</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>9.9</td>
                            <td>1%~4%</td>
                            <td>Weekdays：07:00-05:59 </td>
                            <td>Weekdays：01:00-24:00 </td>
                        </tr>
                        <tr>
                            <td>EUR/NOK</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>9.9</td>
                            <td>1%~4%</td>
                            <td>Weekdays：07:00-05:59 </td>
                            <td>Weekdays：01:00-24:00 </td>
                        </tr>
                        <tr>
                            <td>USD/SEK</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>9.9</td>
                            <td>1%~4%</td>
                            <td>Weekdays：07:00-05:59 </td>
                            <td>Weekdays：01:00-24:00 </td>
                        </tr>
                        <tr>
                            <td>USD/DKK</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>9.9</td>
                            <td>1%~4%</td>
                            <td>Weekdays：07:00-05:59 </td>
                            <td>Weekdays：01:00-24:00 </td>
                        </tr>
                        <tr>
                            <td>NOK/SEK</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>9.9</td>
                            <td>1%~4%</td>
                            <td>Weekdays：07:00-05:59 </td>
                            <td>Weekdays：01:00-24:00 </td>
                        </tr>
                        <tr>
                            <td>USD/ZAR</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>9.9</td>
                            <td>1%~4%</td>
                            <td>Weekdays：07:00-05:59 </td>
                            <td>Weekdays：01:00-24:00 </td>
                        </tr>
                        <tr>
                            <td>USD/NOK</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>9.9</td>
                            <td>1%~4%</td>
                            <td>Weekdays：07:00-05:59 </td>
                            <td>Weekdays：01:00-24:00 </td>
                        </tr>
                        <tr>
                            <td>EUR/TRY</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>9.9</td>
                            <td>1%~4%</td>
                            <td>Weekdays：07:00-05:59 </td>
                            <td>Weekdays：01:00-24:00 </td>
                        </tr>
                        <tr>
                            <td>USD/TRY</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>9.9</td>
                            <td>1%~4%</td>
                            <td>Weekdays：07:00-05:59 </td>
                            <td>Weekdays：01:00-24:00 </td>
                        </tr>
                        <tr>
                            <td>USD/SGD</td>
                            <td>0.01</td>
                            <td>20</td>
                            <td>100,000</td>
                            <td>9.9</td>
                            <td>1%~4%</td>
                            <td>Weekdays：07:00-05:59 </td>
                            <td>Weekdays：01:00-24:00 </td>
                        </tr>
                    </tbody>
                    
                </table>
                </div>

                <p>Remark：Daylight saving time in the United States, Beijing trading time corresponding to 1 hours earlier<br>AGA remind you to consider raising the risk leverage. A relatively small market volatility may be scaled up, have a greater impact on you have deposited or will have to deposit funds, it may be bad for you, may also be beneficial to you. You may lose all the original deposit, and need to deposit additional funds to cover short positions.</p>
                
            </div>
        </div>

        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>