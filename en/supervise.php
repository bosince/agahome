<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title>Security - agafx</title>
    
    <!-- meta -->
    <meta name="description" content="AGA ">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- css -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <!-- load modernizer -->
    <script type="text/javascript" src="assets/js/modernizr/modernizr-2.7.1.js"></script>
    
    <style>
        @media (max-width: 1440px) {
            .max1440-w920{width: 920px;}

        }

        @media (max-width: 1000px){
            .max1000-w100-{width: 100%}
            .max1000-plr15{padding-left: 15px;padding-right: 15px;}
        }

    </style>

</head>

<body>
    
    
    <div id="wrapper">
        
        <!-- header -->
        <?php include 'header.html'; ?>

        <div class="slide h300 bcfff tc pt120">
            <h2 class="cfff fs40  ffwryh">SECURITY SUPERVISION</h2>
            <!-- <h4 class="fs20  mb30  c333  tc">SECURITY SUPERVISION</h4> -->
        </div>


        
        <div   >
            <div class="container tc ffwryh fs16 ">
                <div class="tc mt50">
                    <img src="assets/img/supervise/01.png" alt="" class="max1000-w100-">
                    <!-- <img src="assets/img/supervise/02.png" alt="" class="mt30"> -->
                </div>
                <p class="dib fs22 mt10">With AGA's two major regulatory systems</p>
                <div class="row pt30">
                    <div class="col-xs-6">
                        <img src="assets/img/supervise/03.png" alt="" class="max1000-w100-">
                    </div>
                    <div class="col-xs-6">
                        <img src="assets/img/supervise/04.png" alt="" class="max1000-w100-">
                    </div>
                </div>
                <div class="row mt30 tl mb30">
                    <div class="col-xs-6 br1s666">
                        <p class="ti2 lh25 pr20 pl30 max1000-plr15">Unlike many traditional companies in the OTC market, AGA is more focused on providing customized trading platform solutions to institutional clients and partners in the OTC market, including DMA direct market access, risk management and API liquidity. At the same time, AGA International Finance also provides customized transactions to professional customers.</p>
                    </div>
                    <div class="col-xs-6 ">
                        <ul class="ul1 pl50 pr20 max1000-plr15">
                            <li>AGA is regulated by the UK FCA.</li>
                            <li>AGA is regulated by the US NFA.</li>
                            <li>AGA offers centralized matching options trading and foreign exchange, CFDs, commodity futures.</li>
                            <li>AGA's offices in more than 10 countries around the world.</li>
                        </ul>
                    </div>
                </div>
                <img src="assets/img/supervise/05.png" alt="" class="mt80 w100-">
                <div>
                    <h4 class="fs40 fw7 pt50  lh60 c333  tc">Regulatory advantage</h4>
                    <div class="row mt50">
                        <div class="col-xs-6 col-md-1-5">
                            <div>
                                <img src="assets/img/supervise/07.png" alt="">
                            </div>
                            <div class="mt30">
                                <p class="lh25">Protect investors need and improve the internal supervision of financial institutions as well as market transparency</p>
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-1-5"><div>
                                <img src="assets/img/supervise/08.png" alt="">
                            </div>
                            <div class="mt30">
                                <p class="lh25">British FCA has the perfect financial regulatory system and strict enforcement efforts</p>
                            </div>
                        </div>
                        <div class="col-md-1-5 col-xs-4">
                            <div>
                                <img src="assets/img/supervise/09.png" alt="">
                            </div>
                            <div class="mt30">
                                <p class="lh25">US NFA is the world's most stringent, most able financial regulatory system to protect the investment interests</p>
                            </div>
                        </div>
                        <div class="col-md-1-5 col-xs-4">
                            <div>
                                <img src="assets/img/supervise/10.png" alt="">
                            </div>
                            <div class="mt30">
                                <p class="lh25">Investors can invest in steady financial markets</p>
                            </div>
                        </div>

                        <div class="col-md-1-5 col-xs-4">
                            <div>
                                <img src="assets/img/supervise/11.png" alt="">
                            </div>
                            <div class="mt30">
                                <p class="lh25">British FCA and the US NFA licenses are the investor's double insurance</p>
                            </div>
                        </div>
                    </div>
                </div>
                <img src="assets/img/supervise/05.png" alt="" class="mt80 w100-"  >
                <div class="row mt100 mb100">
                    <div class="col-sm-6 hidden-xs">
                        <img src="assets/img/supervise/06.jpg" alt="" class="max1440-w100">
                    </div>
                    <div class="col-xs-12 col-sm-6 tl">
                    <h4 class="fs40 fw7  lh60 c333 max1440-fs30 max1440-mt-20">Dispute resolution scheme</h4>
                    <!-- <h4 class="fs20  mb10  c333 ">dispte resolution scheme</h4> -->
                    <div>
                        <p class="lh25 fs14">AGA has the UK FCA and the US NFA dual regulation. At the same time, AGA has joined the UK Financial Services Compensation Scheme (FSCS). In the event of accident transaction, Investors will receive £ 50,000 compensation, and it is possible to raise this compensation in full. At the same time, AGA also joined the US Commodity and Futures Trading Commission (CFTC). CFTC as an independent body, it has the primary responsibility and role to protect market participants and investors , guarantee  the openness,competitiveness and financial reliability of futures and options markets.</p>

                    </div>

                    <h4 class="fs40 fw7 pt50  lh60 c333 mt50 max1440-mt-20 max1440-fs30">Against Money Laundering</h4>
                    <!-- <h4 class="fs20  mb10  c333 ">Against Money Laundering</h4> -->
                    <p  class="lh25 fs14">Regarding to illegal FCA and NFA, all licensed companies must have KYC (understand your customers) and AML (anti-money laundering) related company audit policy and legal requirements. Process risk assessment to customers, stipulate against money laundering and anti-terrorism finance, and set concrete requirements.</p>

                    
                </div>
                </div>

                
            </div>
        </div>
        
        <!-- footer -->
        <?php include 'footer.html'; ?>
        
    </div> <!-- wrapper -->

    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="assets/js/jw-base.js"></script>
</body>
</html>